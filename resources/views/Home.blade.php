@extends('layouts.App')
@section('appStyles')

@section('appHeader')
    <!-- .site-header -->
@endsection
@section('appContent')
    <div class="homepage-boxes">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="opening-hours">
                        <h2 class="d-flex align-items-center">Opening Hours</h2>

                        <ul class="p-0 m-0">
                            <li>Monday - Thursday <span>8.00 - 19.00</span></li>
                            <li>Friday <span>8.00 - 18.30</span></li>
                            <li>Saturday <span>9.30 - 17.00</span></li>
                            <li>Sunday <span>9.30 - 15.00</span></li>
                        </ul>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                    <div class="emergency-box">
                        <h2 class="d-flex align-items-center">Emergency</h2>

                        <div class="call-btn button gradient-bg">
                            <a class="d-flex justify-content-center align-items-center" href="#"><img
                                    src="{{asset('images/emergency-call.png')}}"> +34 586 778 8892</a>
                        </div>

                        <p>Lorem ipsum dolor sit amet, cons ectetur adipiscing elit. Donec males uada lorem maximus
                            mauris sceler isque, at rutrum nulla.</p>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-5 mt-5 mt-lg-0">
                    <div class="appointment-box">
                        <h2 class="d-flex align-items-center">Make an Appointment</h2>

                        <form class="d-flex flex-wrap justify-content-between">
                            @csrf
                            <select class="select-department">
                                <option value="value1">Select Department</option>
                                <option value="value2">Select Department 1</option>
                                <option value="value3">Select Department 2</option>
                            </select>

                            <select class="select-doctor">
                                <option>Select Doctor</option>
                                <option>Select Doctor 1</option>
                                <option>Select Doctor 2</option>
                            </select>

                            <input type="text" placeholder="Name">

                            <input type="number" placeholder="Phone No">

                            <input class="button gradient-bg" type="submit" value="Boom Appoitnment">
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="our-departments">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="our-departments-wrap">
                        <h2>Our Departments</h2>

                        <div class="row container-fluid">
                            <div class="row el-element-overlay">

                                @foreach($sps as $spe)
                                    <div class="col-lg-3 col-md-6">
                                        <div class="card">
                                            <div class="el-card-item">
                                                <div class="el-card-avatar el-overlay-1"><img
                                                        src="storage/images/{{ $spe->image }}"
                                                        alt="storage/images/{{ $spe->image }}"/>
                                                    <div class="el-overlay">
                                                        <ul class="el-info">
                                                            <li title="brows of doctors"><a
                                                                    class="btn default btn-outline image-popup-vertical-fit"
                                                                    href="{{URL::to('/doctors')}}"><i
                                                                        class="icon-magnifier"></i></a></li>
                                                            <li title="number of doctors"><a
                                                                    class="btn default btn-outline"
                                                                    href="javascript:void(0);"><i
                                                                        class="fas fa-user-md">{{$spe->doctors_count}}</i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="el-card-content"
                                                     style="max-height: 150px ;min-height: 150px">
                                                    <h3 class="box-title">{{$spe->name}}</h3>
                                                    <small>{{$spe->description}}</small>
                                                    <br/></div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="the-news">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>The News</h2>

                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="the-news-wrap">
                                <figure class="post-thumbnail">
                                    <a href="#"><img src="{{asset('images/news-1.png')}}" alt=""></a>
                                </figure>

                                <header class="entry-header">
                                    <h3>The latest in Medicine</h3>

                                    <div class="post-metas d-flex flex-wrap align-items-center">
                                        <div class="posted-date"><label>Date: </label><a href="#">April 12, 2018</a>
                                        </div>

                                        <div class="posted-by"><label>By: </label><a href="#">Dr. Jake Williams</a>
                                        </div>

                                        <div class="post-comments"><a href="#">2 Comments</a></div>
                                    </div>
                                </header>

                                <div class="entry-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem
                                        maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien.
                                        Suspendisse cursus faucibus finibus. </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="the-news-wrap">
                                <figure class="post-thumbnail">
                                    <a href="#"><img src="{{asset('images/news-2.png')}}" alt=""></a>
                                </figure>

                                <header class="entry-header">
                                    <h3>All you need to know about pills</h3>

                                    <div class="post-metas d-flex flex-wrap align-items-center">
                                        <div class="posted-date"><label>Date: </label><a href="#">April 12, 2018</a>
                                        </div>

                                        <div class="posted-by"><label>By: </label><a href="#">Dr. Jake Williams</a>
                                        </div>

                                        <div class="post-comments"><a href="#">2 Comments</a></div>
                                    </div>
                                </header>

                                <div class="entry-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem
                                        maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien.
                                        Suspendisse cursus faucibus finibus. </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="the-news-wrap">
                                <figure class="post-thumbnail">
                                    <a href="#"><img src="{{asset('images/news-3.png')}}" alt=""></a>
                                </figure>

                                <header class="entry-header">
                                    <h3>Marketing and Medicine</h3>

                                    <div class="post-metas d-flex flex-wrap align-items-center">
                                        <div class="posted-date"><label>Date: </label><a href="#">April 12, 2018</a>
                                        </div>

                                        <div class="posted-by"><label>By: </label><a href="#">Dr. Jake Williams</a>
                                        </div>

                                        <div class="post-comments"><a href="#">2 Comments</a></div>
                                    </div>
                                </header>

                                <div class="entry-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem
                                        maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien.
                                        Suspendisse cursus faucibus finibus. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="subscribe-banner">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <h2>Subscribe to our newsletter</h2>

                    <form>
                        <input type="email" placeholder="E-mail address">
                        <input class="button gradient-bg" type="submit" value="Subscribe">
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('appFooter')


@endsection

@section('appScripts')


    <script type='text/javascript' src='{{asset('js/jquery.js')}}'></script>
    <script type='text/javascript' src='{{asset('js/jquery.collapsible.min.js')}}'></script>
    <script type='text/javascript' src='{{asset('js/swiper.min.js')}}'></script>
    <script type='text/javascript' src='{{asset('js/jquery.countdown.min.js')}}'></script>
    <script type='text/javascript' src='{{asset('js/circle-progress.min.js')}}'></script>
    <script type='text/javascript' src='{{asset('js/jquery.countTo.min.js')}}'></script>
    <script type='text/javascript' src='{{asset('js/jquery.barfiller.js')}}'></script>
    <script type='text/javascript' src='{{asset('js/home.js')}}'></script>

@endsection



