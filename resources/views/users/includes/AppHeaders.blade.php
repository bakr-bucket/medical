<div class="nav-bar">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                <div class="site-branding d-flex align-items-center">
                    <a class="d-block" href="{{URL::to('/')}}" rel="home"><img class="d-block"
                                                                               src="{{asset('images/logo.png')}}"
                                                                               alt="logo"></a>
                </div><!-- .site-branding -->


                <nav class="site-navigation d-flex justify-content-end align-items-center">
                    <ul class="d-flex flex-column flex-lg-row justify-content-lg-end align-items-center">
                        <li class="current-menu-item"><a href="{{URL::to('/')}}">Home</a></li>
                        {{--                        <li><a href="services.html">Services</a></li>--}}
                        <li><a href="#">News</a></li>

                        {{--                        <li >--}}
                        {{--                            <ul class="navbar-nav ml-auto">--}}
                    <!-- Authentication Links -->
                        @guest
                            <li class="call-btn button gradient-bg mt-3 mt-md-0">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="call-btn button gradient-bg mt-3 mt-md-0">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else

                            <li class="call-btn button gradient-bg mt-3 mt-md-0" >
                                <a id="" class="" href="{{URL::to('/')}}" >
                                   Hi &nbsp;&nbsp; {{ Auth::user()->name }} 🤗️🤗️ <span class="caret"></span>
                                </a>
                            </li>
                            <li class="call-btn button gradient-bg mt-3 mt-md-0" >
                                <a class="" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}  &nbsp;&nbsp; &nbsp;&nbsp; 🤔️
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        @endguest

                    </ul>
                </nav><!-- .site-navigation -->

                <div class="hamburger-menu d-lg-none">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div><!-- .hamburger-menu -->
            </div><!-- .col -->
        </div><!-- .row -->


    </div><!-- .container -->
</div><!-- .nav-bar -->

<div class="swiper-container hero-slider">
    <div class="swiper-wrapper">
        <div class="swiper-slide hero-content-wrap" style="background-image: url('{{asset('images/hero.jpg')}}')">
            <div class="hero-content-overlay position-absolute w-100 h-100">
                <div class="container h-100">
                    <div class="row h-100">
                        <div class="col-12 col-lg-6 d-flex flex-column justify-content-center align-items-start">
                            <header class="entry-header">
                                <h1>The Best <br>Medical Services</h1>
                            </header><!-- .entry-header -->

                            <div class="entry-content mt-4">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem
                                    maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse
                                    cursus faucibus finibus.</p>
                            </div><!-- .entry-content -->

                            <footer class="entry-footer d-flex flex-wrap align-items-center mt-4">
                                <a href="#" class="button gradient-bg">Read More</a>
                            </footer><!-- .entry-footer -->
                        </div><!-- .col -->
                    </div><!-- .row -->
                </div><!-- .container -->
            </div><!-- .hero-content-overlay -->
        </div><!-- .hero-content-wrap -->

        <div class="swiper-slide hero-content-wrap" style="background-image: url('{{asset('images/hero.jpg')}}')">
            <div class="hero-content-overlay position-absolute w-100 h-100">
                <div class="container h-100">
                    <div class="row h-100">
                        <div class="col-12 col-lg-6 d-flex flex-column justify-content-center align-items-start">
                            <header class="entry-header">
                                <h1>The Best <br>Medical Services</h1>
                            </header><!-- .entry-header -->

                            <div class="entry-content mt-4">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem
                                    maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse
                                    cursus faucibus finibus.</p>
                            </div><!-- .entry-content -->

                            <footer class="entry-footer d-flex flex-wrap align-items-center mt-4">
                                <a href="#" class="button gradient-bg">Read More</a>
                            </footer><!-- .entry-footer -->
                        </div><!-- .col -->
                    </div><!-- .row -->
                </div><!-- .container -->
            </div><!-- .hero-content-overlay -->
        </div><!-- .hero-content-wrap -->

        <div class="swiper-slide hero-content-wrap" style="background-image: url('{{asset('images/hero.jpg')}}')">
            <div class="hero-content-overlay position-absolute w-100 h-100">
                <div class="container h-100">
                    <div class="row h-100">
                        <div class="col-12 col-lg-6 d-flex flex-column justify-content-center align-items-start">
                            <header class="entry-header">
                                <h1>The Best <br>Medical Services</h1>
                            </header><!-- .entry-header -->

                            <div class="entry-content mt-4">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem
                                    maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse
                                    cursus faucibus finibus.</p>
                            </div><!-- .entry-content -->

                            <footer class="entry-footer d-flex flex-wrap align-items-center mt-4">
                                <a href="#" class="button gradient-bg">Read More</a>
                            </footer><!-- .entry-footer -->
                        </div><!-- .col -->
                    </div><!-- .row -->
                </div><!-- .container -->
            </div><!-- .hero-content-overlay -->
        </div><!-- .hero-content-wrap -->
    </div><!-- .swiper-wrapper -->

    <div class="pagination-wrap position-absolute w-100">
        <div class="swiper-pagination d-flex flex-row flex-md-column"></div>
    </div><!-- .pagination-wrap -->
</div><!-- .hero-slider -->
