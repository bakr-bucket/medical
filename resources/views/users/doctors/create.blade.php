@extends('layouts.master')

@section('masterStyles')

    {{--    <link rel="stylesheet" type="text/css"--}}
    {{--          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}

    {{--    <!-- Swiper CSS -->--}}
    {{--    <link rel="stylesheet" href="{{asset('css/swiper.min.css')}}">--}}

    {{--    <!-- Styles -->--}}
    {{--    <link rel="stylesheet" href="{{asset('css/homeStyle.css')}}">--}}

    {{--    <script type='text/javascript' src='{{asset('js/jquery.js')}}'></script>--}}
    {{--    <script type='text/javascript' src='{{asset('js/swiper.min.js')}}'></script>--}}

    {{--    <script type='text/javascript' src='{{asset('js/home.js')}}'></script>--}}

    @yield("appStyles")
@endsection

<header class="site-header">
    @section('masterHeader')

        @include('users.includes.AppHeaders')
        @yield('appHeader')
    @endsection
</Header>

@section('masterContent')
    <br><br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Doctor Register ') }}</div>

                    <div class="card-body">
                        {!! Form::open(array('action' => 'DoctorController@store','method' => 'post','files' => true)) !!}

                        {{--                            Name--}}
                        <div class="form-group row">
                            {{ Form::label('name', 'Doctor Name', array('class' => 'col-md-4 col-form-label text-md-right'))}}
                            <div class="col-md-6">
                                {{Form::text('officeName', old('officeName'),array('class' => 'form-control','required' => 'required','autofocus'=>'autofocus'))}}
                            </div>
                        </div>
                        {{--                            phone --}}
                        <div class="form-group row">
                            {{ Form::label('Phone', 'Doctor Phone', array('class' => 'col-md-4 col-form-label text-md-right'))}}
                            <div class="col-md-6">
                                {{Form::number('phone', old('phone'),array('class' => 'form-control','required' => 'required','autofocus'=>'autofocus'))}}
                            </div>
                        </div>
                        {{--fees--}}
                        <div class="form-group row">
                            {{ Form::label('fees', 'Fees of Session', array('class' => 'col-md-4 col-form-label text-md-right'))}}

                            <div class="col-md-6">
                                {{Form::number('fees', old('fees'),array('class' => 'form-control','required' => 'required','autofocus'=>'autofocus'))}}
                            </div>
                        </div>
                        {{--                        specialities--}}

                        <div class="form-group row">
                            {{ Form::label('fees', 'Fees of Session', array('class' => 'col-md-4 col-form-label text-md-right'))}}

                            <div class="col-md-6">
                                {!!
                                Form::select('speciality',
                                $specialty,null,
                                array('class' => 'form-control','required' => 'required','autofocus'=>'autofocus'))
                                !!}

                            </div>
                        </div>

                        {{--location--}}
                        <div class="form-group row">
                            {{ Form::label('location', 'Doctor office', array('class' => 'col-md-4 col-form-label text-md-right'))}}
                            <div class="col-md-6">
                                {{Form::text('location', old('location'),array('class' => 'form-control','required' => 'required'))}}
                            </div>
                        </div>

                        {{--description--}}
                        <div class="form-group row">
                            {{ Form::label('description', 'description', array('class' => 'col-md-4 col-form-label text-md-right'))}}

                            <div class="col-md-6">
                                {{Form::text('description', old('description'),array('class' => 'form-control','required' => 'required','autofocus'=>'autofocus'))}}

                            </div>
                        </div>

                        {{--photo--}}
                        <div class="form-group row">
                            {{ Form::label('photo', 'Doctor photo', array('class' => 'col-md-4 col-form-label text-md-right'))}}


                            <div class="col-md-6">
                                {{Form::file('photo',old('photo'),array('class' => 'form-control','required' => 'required','autofocus'=>'autofocus'))}}

                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register Doctor') }}
                                </button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@section('masterFooter')
    @yield("appFooter")
@endsection

@section('masterScripts')
    @yield("appScripts")
@endsection
