@extends('layouts.master')

@section('masterStyles')

    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{asset('css/swiper.min.css')}}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/homeStyle.css')}}">

    <script type='text/javascript' src='{{asset('js/jquery.js')}}'></script>
    <script type='text/javascript' src='{{asset('js/swiper.min.js')}}'></script>

    <script type='text/javascript' src='{{asset('js/home.js')}}'></script>
    @yield("appStyles")
@endsection

<header class="site-header">
    @section('masterHeader')

    @include('users.includes.AppHeaders')
        @yield('appHeader')
    @endsection
</Header>

@section('masterContent')
    <div id="app">
        @yield('appContent')
    </div>
@endsection



@section('masterFooter')
    @yield("appFooter")
@endsection

@section('masterScripts')
    @yield("appScripts")
@endsection
