<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('layouts.inc.configuration')
    @include('layouts.inc.styles')
    {{-- For additional styles from any page--}}
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{asset('css/swiper.min.css')}}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/homeStyle.css')}}">

    <script type='text/javascript' src='{{asset('js/jquery.js')}}'></script>
    <script type='text/javascript' src='{{asset('js/swiper.min.js')}}'></script>

    <script type='text/javascript' src='{{asset('js/home.js')}}'></script>

    @yield('masterStyles')
</head>

<body class="fix-header fix-sidebar card-no-border">


@yield('masterHeader')


@yield('masterContent')


<section>
    <footer class="site-footer">
        <div class="footer-widgets">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="foot-about">
                            <h2><a href="#"><img src="{{assert('images/logo.png')}}" alt=""></a></h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus
                                mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien.</p>
                            <p class="copyright">
                                Copyright &copy;<script type="fe875b14305a1946bb53c095-text/javascript">
                                    document.write(new Date().getFullYear());
                                </script>
                                All rights reserved | This template is made with <i class="fa fa-heart"
                                                                                    aria-hidden="true"></i> by <a
                                    href="https://colorlib.com" target="_blank">Colorlib</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 mt-5 mt-md-0">
                        <div class="foot-contact">
                            <h2>Contact</h2>
                            <ul class="p-0 m-0">
                                <li><span>Addtress:</span>Mitlton Str. 26-27 London UK</li>
                                <li><span>Phone:</span>+53 345 7953 32453</li>
                                <li><span>Email:</span><a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                          data-cfemail="4d3422383f202c24210d2a202c2421632e2220">[email&#160;protected]</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 mt-5 mt-md-0">
                        <div class="foot-links">
                            <h2>Usefull Links</h2>
                            <ul class="p-0 m-0">
                                <li><a href="index.html">Home</a></li>
                                <li><a href="about.html">About us</a></li>
                                <li><a href="#">Departments</a></li>
                                <li><a href="contact.html">Contact</a></li>
                                <li><a href="news.html">FAQ</a></li>
                                <li><a href="services.html">Testimonials</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    @yield('masterFooter')
</section>

@include('layouts.inc.scripts')
@yield('masterScripts')
</body>

</html>
