<!-- Bootstrap Core CSS -->
<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<!--alerts CSS -->
<link href="{{asset('assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<!-- AdminPro style -->
<link href="{{asset('css/style.css')}}" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="{{asset('css/colors/blue.css')}}" id="theme" rel="stylesheet">
<!-- For customization -->
<link rel="stylesheet" href="{{asset('css/custom.css')}}">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
