
<script>
    // It is very useful with javascript
    var url = "{{url('/')}}";

</script>

<!-- For Routing in JavaScript -->
{{--<script src="{{asset('js/laroute.js')}}"></script>--}}
<!-- For Jquery, Bootstrap, React -->
<script src="{{asset('js/app.js')}}"></script>

<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}"></script>

<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}"></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- ============= sweet alert  ================================================= -->
<script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}"></script>
<!-- Style switcher -->
<script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>
{{--<script>--}}
{{--    // It is very useful with javascript--}}
{{--    var url = "{{url('/')}}";--}}

{{--</script>--}}

{{--<!-- For Routing in JavaScript -->--}}
{{--<!-- For Jquery, Bootstrap, React -->--}}
{{--<script src="{{asset('js/app.js')}}"></script>--}}
{{--<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>--}}
{{--<!-- Bootstrap tether Core JavaScript -->--}}
{{--<script src="{{asset('assets/plugins/popper/popper.min.js')}}"></script>--}}
{{--<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>--}}

{{--<!-- slimscrollbar scrollbar JavaScript -->--}}
{{--<script src="{{asset('js/jquery.slimscroll.js')}}"></script>--}}

{{--<!--Wave Effects -->--}}
{{--<script src="{{asset('js/waves.js')}}"></script>--}}
{{--<!--Menu sidebar -->--}}
{{--<script src="{{asset('js/sidebarmenu.js')}}"></script>--}}
{{--<!--stickey kit -->--}}
{{--<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>--}}
{{--<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>--}}
{{--<!-- ============= sweet alert  ================================================= -->--}}
{{--<script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>--}}
{{--<script src="{{asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js')}}"></script>--}}
{{--<!--Custom JavaScript -->--}}
{{--<script src="{{asset('js/custom.min.js')}}"></script>--}}

{{--<!-- Magnific popup JavaScript -->--}}
{{--<script src="{{asset('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>--}}
{{--<script src="{{asset('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script>--}}

{{--<!-- ============================================================== -->--}}
{{--<!-- Style switcher -->--}}
{{--<!-- ============================================================== -->--}}
{{--<script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>--}}

{{--<!-- Style switcher -->--}}
{{--<script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>--}}
{{--<!-- All Jquery -->--}}
{{--<!-- ============================================================== -->--}}

{{--<!--Custom JavaScript -->--}}
