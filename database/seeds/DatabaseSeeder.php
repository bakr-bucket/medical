<?php

use App\Specialty;
use Illuminate\Database\Seeder;
use App\Doctor;
use App\Reservation;
use App\Transaction;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(UsersTableSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        Reservation::truncate();
        Doctor::truncate();
        Transaction::truncate();
        Specialty::truncate();

        $usersN=50;
        $resN=50;
        $doctorN=20;
        $transactionsN=40;
        $sp=10;


        factory(User::class,$usersN)->create();
        factory(Specialty::class,$sp)->create();
        factory(Reservation::class,$resN)->create();
        factory(Doctor::class,$doctorN)->create();
        factory(Transaction::class,$transactionsN)->create();

    }
/*
 *
 *

*/
}
