<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Specialty;
use App\User;
use App\Doctor;
use App\Transaction;
use App\Reservation;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
//        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'verified' => $faker->randomElement([User::VERIFIED_USER, User::UNVERIFIED_USER]),
    ];
});
$factory->define(Specialty::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'image' => $faker->image('public/storage/images', 400, 300, null, false),
        'description' => $faker->paragraph(1),
    ];
});

$factory->define(Reservation::class, function (Faker $faker) {
    return [
        'time' => $faker->dateTimeThisCentury($max = 'now', $timezone = null),
        'description' => $faker->paragraph(1),
        'patient_id' => User::all()->random()->id,
    ];
});


$factory->define(Doctor::class, function (Faker $faker) {
    return [
        'officeName' => $faker->name,
        'phone'=>$faker->phoneNumber,
        'photo' => $faker->image('public/storage/images/doc', 400, 300, null, false),
        'description' => $faker->paragraph(3),
        'location' => $faker->address,
        'fees' => $faker->numberBetween(50, 100),
        'user_id' => $faker->unique()->numberBetween(1,50),
        'specialty_id' => Specialty::all()->random()->id,
    ];
});


$factory->define(Transaction::class, function (Faker $faker) {
    $patient = Reservation::all()->random()->patient_id;
    $doctor = User::all()->except($patient)->random();
    return [
        'type' => $faker->randomElement([Transaction::APPROVED, Transaction::REJECTED]),
        'doctor_id' => $doctor->id,
        'reservation_id' => Reservation::all()->random()->id,
    ];
});

