<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    //
    protected $table='users';

    public function reservations(){
        return $this->hasMany(Reservation::class);
    }

}
