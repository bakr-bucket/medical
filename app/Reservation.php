<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    //
    protected $fillable=[
        'time',
        'description',
        'patient_id',
    ];

    public function patient(){
        return $this->belongsTo(Patient::class);
    }
    public function transactions(){
        return $this->hasMany(Transaction::class);
    }

}
