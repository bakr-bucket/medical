<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialty extends Model
{
    protected $fillable=[
        'name',
        'image',
        'description'
    ];

    public function doctors()
    {
        return $this->hasMany(Doctor::class);
    }
}
