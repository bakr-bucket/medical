<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    protected $fillable = [
        'type',
        'reservation_id',
        'doctor_id'
    ];
    public const APPROVED = '1';
    public const REJECTED = '0';

    public function isApprove()
    {
        return $this->type = self::APPROVED;
    }

    public function isRejected()
    {
        return $this->type = self::REJECTED;
    }

    public function doctor()
    {
        return $this->belongsTo(Doctor::class);
    }

    public function reservation()
    {
        return $this->belongsTo(Reservation::class);
    }
}
