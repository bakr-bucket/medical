<?php

namespace App\Http\Controllers;

use App\Doctor;
use App\Specialty;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\DocBlock;
use PhpParser\Comment\Doc;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctors = Doctor::all();
        return view('users.doctors.index', compact('doctors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $specialty = Specialty::pluck('name', 'id')->toArray();
        return view('users.doctors.create', compact('specialty'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $specialty = Specialty::find($request->speciality);
        $user = $request->user();

        $roles = [
            'officeName' => 'required',
            'phone' => 'required',
            'fees' => 'required',
            'location' => 'required',
            'description' => 'required',
        ];

        $this->validate($request, $roles);

        $data = $request->all();
//
        $data['user_id'] = $user->id;
        $data['speciality_id'] = $request->speciality;

        if ($image = $request->file('photo')) {
            $name = $image->getClientOriginalName();
            $image->move('storage/images/doc', $name);
            $data['photo'] = $name;
        } else {
            return redirect()->route('users.cars.create')->with('fail', 'your question has been submitted');
        }

        $doc = $specialty->doctors()->create($data);

        $doc->user()->associate($user)->save();


        return redirect('/doctors' . '/' . $doc->id)->with('success', 'your question has been submitted');

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Doctor $doctor
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {
//        return ($doctor->specialty->name);
        $patient = $doctor->transactions()
            ->with('reservation.patient')->get()
            ->pluck('reservation.patient')
            ->unique('id')
            ->count();
        $u = User::where('id', $doctor->user_id)->get();
        $email = $u[0]->email;
        $doctor->setAttribute('email', $email);
        $doctor->setAttribute('patientNumber', $patient);
//             return $doctor;
        return view('users.doctors.profile', compact('doctor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Doctor $doctor
     * @return \Illuminate\Http\Response
     */
    public function edit(Doctor $doctor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Doctor $doctor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctor $doctor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Doctor $doctor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor)
    {
        //
    }
}
