<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $fillable = [
        'officeName',
        'phone',
        'photo',
        'description',
        'location',
        'fees',
        'user_id',
        'specialty_id',
    ];



    public function transactions()
    {
        return $this->hasMany(Transaction::class);

    }
    public function specialty(){
        return $this->belongsTo(Specialty::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }

}
