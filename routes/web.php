<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('/','SpecialtyController');

Auth::routes();

Route::get('/create', function () {
    return view('users.doctors.create');
});

Route::get('/patient', function () {
    return view('users.patients.patient00');
});
Route::get('/doctor', function () {
    return view('users.doctors.profile');
});

Route::resource('/doctors','DoctorController');


//Route::get('/home', 'HomeController@index')->name('home');
